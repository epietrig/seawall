

"use strict";

var seawall = SAGE2_WebGLApp.extend({

	init: function(data) {
		// Create a canvas into the DOM
		this.WebGLAppInit('canvas', data);

		// Set the background to black
		this.element.style.backgroundColor = 'black';

		this.video = document.createElement( 'video' );
		//this.video.loop = true;
		//this.video.src = this.resrcPath + "videos/tsunami.mp4";
		//this.video.load();
		//this.playing = null;

		this.videoImage = document.createElement( 'canvas' );
		// Adhitya: This is very important! If you don set this properly, then the only a small top-left portion of the video might be seen
		this.videoImage.width = 2156;
		this.videoImage.height = 960;

		this.videoImageContext = this.videoImage.getContext( '2d' );
		this.videoImageContext.fillStyle = '#000000';
		this.videoImageContext.fillRect( 0, 0, this.videoImage.width, this.videoImage.height );

		this.videoTexture = new THREE.Texture( this.videoImage );
		this.videoTexture.minFilter = THREE.LinearFilter;
		this.videoTexture.magFilter = THREE.LinearFilter;

		this.renderer = null;
		this.camera   = null;
		this.scene    = null;
		this.ready    = null;

		this.cameraCube = null;
		this.sceneCube  = null;
		this.dragging   = null;
		this.rotating   = null;

		this.element.id = "div" + data.id;
		this.frame  = 0;
		this.width  = this.element.clientWidth;
		this.height = this.element.clientHeight;
		this.dragging = false;
		this.ready    = false;
		this.rotating = false;

		// build the app
		this.initialize(data.date);

		this.addControls();
		//this.resizeCanvas();

		this.setColor({p: this.state.p});
		// Get the value and update the app as needed
		this.serverDataGetValue("globalColor", "getValue");
		// Subscribe to the update for this variable
		this.serverDataSubscribeToValue('globalColor', 'subscribeToColor');

		this.refresh(data.date);
	},

	initialize: function(date) {
		console.log("initialize ctm");
		// CAMERA
		this.camera = new THREE.PerspectiveCamera(25, this.width / this.height, 1, 10000);
		this.camera.position.set(0, 0, 400);

		this.orbitControls = new THREE.OrbitControls(this.camera, this.element);
		//this.orbitControls.maxPolarAngle = Math.PI / 2;
		this.orbitControls.minDistance = 200;
		this.orbitControls.maxDistance = 500;
		this.orbitControls.autoRotate  = true;
		this.orbitControls.zoomSpeed   = 1.0;
		this.orbitControls.autoRotateSpeed = 0.0; // 30 seconds per round when fps is 60

		// SCENE
		this.scene = new THREE.Scene();

		// SKYBOX
		this.sceneCube  = new THREE.Scene();
		this.cameraCube = new THREE.PerspectiveCamera(25, this.width / this.height, 1, 10000);
		this.sceneCube.add(this.cameraCube);

		// Texture cube
		var cubeLoader = new THREE.CubeTextureLoader();
		var r    = this.resrcPath + "textures/";
		var urls = [r + "px.jpg", r + "nx.jpg", r + "py.jpg", r + "ny.jpg", r + "pz.jpg", r + "nz.jpg"];
		var textureCube = cubeLoader.load(urls);

		var shader = THREE.ShaderLib.cube;
		shader.uniforms.tCube.value = textureCube;

		var material = new THREE.MeshPhongMaterial();

		var mesh = new THREE.Mesh(new THREE.BoxGeometry(100, 100, 100), material);
		this.sceneCube.add(mesh);

		// LIGHTS
		var light = new THREE.PointLight(0xffffff, 1);
		light.position.set(2, 5, 1);
		light.position.multiplyScalar(30);
		this.scene.add(light);

		var light2 = new THREE.PointLight(0xffffff, 0.75);
		light2.position.set(-12, 4.6, 2.4);
		light2.position.multiplyScalar(30);
		this.scene.add(light2);

		this.scene.add(new THREE.AmbientLight(0x050505));

		// Loader

		var position = new THREE.Vector3(0, 0, 0);
		var scale    = new THREE.Vector3(100, 100, 100);

		var _this = this;
		
		
		_this.tsunami = {}
		_this.tsunami.geometry = new THREE.SphereGeometry( 0.292, 32, 32 );
		//_this.tsunami.geometry = new THREE.PlaneGeometry( 240, 100, 20, 4 );		
		_this.tsunami.material = new THREE.MeshBasicMaterial({
			map         : _this.videoTexture,
			side        : THREE.DoubleSide,
		 	opacity     : 0.2,
			transparent : true,
			depthWrite  : false,
			overdraw    : true,
		});
		_this.tsunami.mesh = new THREE.Mesh(_this.tsunami.geometry, _this.tsunami.material);
		_this.tsunami.mesh.position.copy(position);
		_this.tsunami.mesh.scale.copy(scale);
		_this.scene.add(_this.tsunami.mesh);
		_this.refresh(date);		


		_this.earth = {};
		_this.earth.geometry = new THREE.SphereGeometry( 0.29, 32, 32 );
		var earthImage = this.resrcPath + "images/" + "NE2_ps_flat_v2.jpg";
		
		var loader = new THREE.TextureLoader().load(earthImage, function(texture){
		    _this.earth.texture = texture;
			_this.video.play();
			//_this.earth.material = new THREE.MeshBasicMaterial({map: _this.videoTexture});
		    _this.earth.material = new THREE.MeshBasicMaterial({map: _this.earth.texture});
		    _this.earth.mesh = new THREE.Mesh( _this.earth.geometry, _this.earth.material );
			_this.earth.mesh.position.copy(position);
			_this.earth.mesh.scale.copy(scale);
			_this.scene.add(_this.earth.mesh);
		    _this.refresh(date);

		} );

		// RENDERER
		if (this.renderer == null) {
			this.renderer = new THREE.WebGLRenderer({
				canvas: this.canvas,
				antialias: true
			});
			this.renderer.setSize(this.width, this.height);
			this.renderer.autoClear = false;

			this.renderer.gammaInput  = true;
			this.renderer.gammaOutput = true;
		}


		this.ready = true;

		// draw!
		this.resize(date);
	},

	load: function(date) {
		// nothing
	},

	draw: function(date) {
		if (this.ready) {

			if ( this.video.readyState === this.video.HAVE_ENOUGH_DATA ) 
			{
				this.videoImageContext.drawImage( this.video, 0, 0 );
				if ( this.videoTexture ) 
					this.videoTexture.needsUpdate = true;
			}

			this.orbitControls.update();
			this.cameraCube.rotation.copy(this.camera.rotation);

			this.renderer.clear();
			this.renderer.render(this.sceneCube, this.cameraCube);
			this.renderer.render(this.scene, this.camera);
		}
	},

	// Local Threejs specific resize calls.
	resizeApp: function(resizeData) {
		//console.log(resizeData);
		if (this.renderer != null && this.camera != null) {
			this.renderer.setSize(this.canvas.width, this.canvas.height);

			this.camera.setViewOffset(this.sage2_width, this.sage2_height,
				resizeData.leftViewOffset, resizeData.topViewOffset,
				resizeData.localWidth, resizeData.localHeight);
			this.cameraCube.setViewOffset(this.sage2_width, this.sage2_height,
				resizeData.leftViewOffset, resizeData.topViewOffset,
				resizeData.localWidth, resizeData.localHeight);
		}
	},

	event: function(eventType, position, user_id, data, date) {
		if (this.ready) {
			if (eventType === "pointerPress" && (data.button === "left")) {
				this.dragging = true;
				this.orbitControls.mouseDown(position.x, position.y, 0);				

			} else if (eventType === "pointerMove" && this.dragging) {
				this.orbitControls.mouseMove(position.x, position.y);
				this.refresh(date);
			} else if (eventType === "pointerRelease" && (data.button === "left")) {
				this.dragging = false;
			}

			if (eventType === "pointerScroll") {
				this.orbitControls.scale(data.wheelDelta);
				this.refresh(date);
			}

			if (eventType === "keyboard") {
				if (data.character === " ") {
					this.rotating = !this.rotating;
					this.orbitControls.autoRotate = this.rotating;
					this.refresh(date);
				}
			}

			if (eventType === "specialKey") {
				if (data.code === 37 && data.state === "down") { // left
					this.orbitControls.pan(this.orbitControls.keyPanSpeed, 0);
					this.orbitControls.update();
					this.refresh(date);
				} else if (data.code === 38 && data.state === "down") { // up
					this.orbitControls.pan(0, this.orbitControls.keyPanSpeed);
					this.orbitControls.update();
					this.refresh(date);
				} else if (data.code === 39 && data.state === "down") { // right
					this.orbitControls.pan(-this.orbitControls.keyPanSpeed, 0);
					this.orbitControls.update();
					this.refresh(date);
				} else if (data.code === 40 && data.state === "down") { // down
					this.orbitControls.pan(0, -this.orbitControls.keyPanSpeed);
					this.orbitControls.update();
					this.refresh(date);
				}
			} else if (eventType === "widgetEvent") {
				switch (data.identifier) {
					case "Up":
						// up
						this.orbitControls.pan(0, this.orbitControls.keyPanSpeed);
						this.orbitControls.update();
						break;
					case "Down":
						// down
						this.orbitControls.pan(0, -this.orbitControls.keyPanSpeed);
						this.orbitControls.update();
						break;
					case "Left":
						// left
						this.orbitControls.pan(this.orbitControls.keyPanSpeed, 0);
						this.orbitControls.update();
						break;
					case "Right":
						// right
						this.orbitControls.pan(-this.orbitControls.keyPanSpeed, 0);
						this.orbitControls.update();
						break;
					case "ZoomIn":
						this.orbitControls.scale(4);
						break;
					case "ZoomOut":
						this.orbitControls.scale(-4);
						break;
					case "Loop":
						this.rotating = !this.rotating;
						this.orbitControls.autoRotate = this.rotating;
						break;
					default:
						console.log("No handler for:", data.identifier);
						return;
				}
				this.refresh(date);
			}
			var cameraPosition = {
									p: {
										x:this.camera.position.x,
										y:this.camera.position.y,
										z:this.camera.position.z
									}
								};
			//console.log(cameraPosition);
			this.setColor(cameraPosition);
		}
	},

	setVideo: function(responseObject) {
		this.video.pause();
		this.video.src = this.resrcPath + responseObject.video.name;
		this.video.play();
	},

	getContextEntries: function() {
		var entries = [];
		var entry;

		entry = {};
		entry.description = "yA31";
		entry.callback    = "setVideo";
		entry.parameters  = { video: {name: "videos/aprox_ya31.webm"} };
		entry.entryColor  = "lightblue";
		entries.push(entry);

		entry = {};
		entry.description = "yb31";
		entry.callback    = "setVideo";
		entry.parameters  = { video: {name: "videos/aprox_yb31.webm"} };
		entry.entryColor  = "lightyellow";
		entries.push(entry);

		entry = {};
		entry.description = "yc31";
		entry.callback    = "setVideo";
		entry.parameters  = { video: {name: "videos/aprox_yc31.webm"} };
		entry.entryColor  = "lightpink";
		entries.push(entry);

		entry = {};
		entry.description = "ya31_caribbean";
		entry.callback    = "setVideo";
		entry.parameters  = {  video: {name: "videos/ya31_caribbean.webm"} };
		entry.entryColor  = "lightgreen";
		entries.push(entry);

		entry = {};
		entry.description = "ya31_indeanocean.webm";
		entry.callback    = "setVideo";
		entry.parameters  = {  video: {name: "videos/ya31_indeanocean.webm"} };
		entry.entryColor  = "white";
		entries.push(entry);

		entry = {};
		entry.description = "ya31_southatlantic";
		entry.callback    = "setVideo";
		entry.parameters  = {  video: {name: "videos/ya31_southatlantic.webm"} };
		entry.entryColor  = "orange";
		entries.push(entry);

		return entries;
	},

	addControls: function(){
		this.controls.addButton({type: "prev", position: 1, identifier: "Left"});
		this.controls.addButton({type: "next", position: 7, identifier: "Right"});
		this.controls.addButton({type: "up-arrow", position: 4, identifier: "Up"});
		this.controls.addButton({type: "down-arrow", position: 10, identifier: "Down"});
		this.controls.addButton({type: "zoom-in", position: 12, identifier: "ZoomIn"});
		this.controls.addButton({type: "zoom-out", position: 11, identifier: "ZoomOut"});
		this.controls.addButton({type: "loop", position: 2, identifier: "Loop"});
		this.controls.finishedAddingControls();
	},

	changeColor: function(responseObject) {
		// update the state of this app
		this.state.p = responseObject.p;
		//console.log("Changing position to this" + this.state.p);
		// update the color of the DOM element (a div)
		this.camera.position.set(this.state.p.x, this.state.p.y, this.state.p.z);
		this.refresh(responseObject.date);
		// synchronized the clients of this app
		this.SAGE2Sync(true);
	},


	getValue: function(value) {
		//console.log('Got a value', value);
		// Change the color of this app
		this.changeColor({p: value});
	},

	subscribeToColor: function(newColor) {
		//console.log('Got an update', newColor);
		// Change the color of this app
		this.changeColor({p: newColor});
	},

	setColor: function(responseObject) {
		// update the state of this app
		this.state.p = responseObject.p;
		// update the color of the DOM element (a div)
		this.camera.position.set(this.state.p.x, this.state.p.y, this.state.p.z);
		this.refresh(responseObject.date);
		// it's a change from a user action, so tell the other app instances
		this.serverDataSetValue("globalColor", this.state.p, "Two Color");
		// synchronized the clients of this app
		this.SAGE2Sync(true);
	}

});
