"use strict";

/* global  */

var tsunamiVideoPlayer = SAGE2_App.extend({
	init: function(data) {
		// Create div into the DOM
		this.SAGE2Init("video", data);
		// Set the DOM id
		this.element.id = "div_" + data.id;
		// Set the background to black
		this.element.style.backgroundColor = 'black';

		// move and resize callbacks
		this.resizeEvents = "continuous"; // onfinish
		// this.moveEvents   = "continuous";

		this.element.loop = true;
		//this.element.src = this.resrcPath + "videos/yA31s.mp4";
		//this.element.pause();

		// Not adding controls but making the default buttons available
		this.controls.finishedAddingControls();
		this.enableControls = true;
		this.videoStatus = false;

		//this.setColor({playing: this.state.playing});
		// Get the value and update the app as needed
		this.serverDataGetValue("globalVideo", "getValue");
		// Subscribe to the update for this variable
		this.serverDataSubscribeToValue('globalVideo', 'subscribeToVideo');

	},

	load: function(date) {
		console.log('tsunamiVideoPlayer> Load with state value', this.state.value);
	
		this.refresh(date);
	},

	draw: function(date) {
		console.log('tsunamiVideoPlayer> Draw with state value', this.state.value);
	},

	resize: function(date) {
		// Called when window is resized
		this.refresh(date);
	},

	move: function(date) {
		// Called when window is moved (set moveEvents to continuous)
		this.refresh(date);
	},

	quit: function() {
		// Make sure to delete stuff (timers, ...)
	},

	event: function(eventType, position, user_id, data, date) {
		if (eventType === "pointerPress" && (data.button === "left")) {
			// click
		} else if (eventType === "pointerMove" && this.dragging) {
			// move
		} else if (eventType === "pointerRelease" && (data.button === "left")) {
			// click release
		} else if (eventType === "pointerScroll") {
			// Scroll events for zoom
		} else if (eventType === "widgetEvent") {
			// widget events
		} else if (eventType === "keyboard") {
			if (data.character === "m") {
				this.refresh(date);
			}
		} else if (eventType === "specialKey") {
			if (data.code === 37 && data.state === "down") {
				// left
				this.refresh(date);
			} else if (data.code === 38 && data.state === "down") {
				// up
				this.refresh(date);
			} else if (data.code === 39 && data.state === "down") {
				// right
				this.refresh(date);
			} else if (data.code === 40 && data.state === "down") {
				// down
				this.refresh(date);
			}
		}
	},

	changeVideo: function(responseObject) {
		this.state.playing = responseObject.playing;
		this.element.play();
		//this.videoStatus = true;
		// synchronized the clients of this app
		this.SAGE2Sync(true);
	},


	getValue: function(value) {
		this.changeVideo({playing: value});
	},

	subscribeToVideo: function(newColor) {
		this.changeVideo({playing: newColor});
	},

	setColor: function(responseObject) {
		// update the state of this app
		this.element.play();
		this.state.playing = responseObject.playing;
		this.refresh(responseObject.date);
		// it's a change from a user action, so tell the other app instances
		this.serverDataSetValue("globalVideo", this.state.playing, "Two Color");
		// synchronized the clients of this app
		this.SAGE2Sync(true);
	},



	setVideo: function(responseObject) {
		this.element.src = this.resrcPath + responseObject.video.name;
		this.element.load();
		this.element.pause();
		//this.element.play();
		this.element.loop = true;
	},

	getContextEntries: function() {
		var entries = [];
		var entry;

		entry = {};
		entry.description = "yA11p";
		entry.callback    = "setVideo";
		entry.parameters  = { video: {name: "videos/yA11p.mp4"} };
		entry.entryColor  = "lightblue";
		entries.push(entry);

		entry = {};
		entry.description = "yA11s";
		entry.callback    = "setVideo";
		entry.parameters  = { video: {name: "videos/yA11s.mp4"} };
		entry.entryColor  = "lightyellow";
		entries.push(entry);

		entry = {};
		entry.description = "yA31p";
		entry.callback    = "setVideo";
		entry.parameters  = { video: {name: "videos/yA31p.mp4"} };
		entry.entryColor  = "lightpink";
		entries.push(entry);

		entry = {};
		entry.description = "yA31s";
		entry.callback    = "setVideo";
		entry.parameters  = {  video: {name: "videos/yA31s.mp4"} };
		entry.entryColor  = "lightgreen";
		entries.push(entry);

		entry = {};
		entry.description = "yA32p";
		entry.callback    = "setVideo";
		entry.parameters  = {  video: {name: "videos/yA32p.mp4"} };
		entry.entryColor  = "white";
		entries.push(entry);

		entry = {};
		entry.description = "yA32s";
		entry.callback    = "setVideo";
		entry.parameters  = {  video: {name: "videos/yA32s.mp4"} };
		entry.entryColor  = "white";
		entries.push(entry);

		entry = {};
		entry.description = "yB11p";
		entry.callback    = "setVideo";
		entry.parameters  = {  video: {name: "videos/yB11p.mp4"} };
		entry.entryColor  = "orange";
		entries.push(entry);

		entry.description = "yB11s";
		entry.callback    = "setVideo";
		entry.parameters  = { video: {name: "videos/yB11s.mp4"} };
		entry.entryColor  = "lightblue";
		entries.push(entry);

		entry = {};
		entry.description = "yB31p";
		entry.callback    = "setVideo";
		entry.parameters  = { video: {name: "videos/yB31p.mp4"} };
		entry.entryColor  = "lightyellow";
		entries.push(entry);

		entry = {};
		entry.description = "yB31s";
		entry.callback    = "setVideo";
		entry.parameters  = { video: {name: "videos/yB31s.mp4"} };
		entry.entryColor  = "lightpink";
		entries.push(entry);

		entry = {};
		entry.description = "yC11p";
		entry.callback    = "setVideo";
		entry.parameters  = {  video: {name: "videos/yC11p.mp4"} };
		entry.entryColor  = "white";
		entries.push(entry);

		entry = {};
		entry.description = "yC11s";
		entry.callback    = "setVideo";
		entry.parameters  = {  video: {name: "videos/yC11s.mp4"} };
		entry.entryColor  = "orange";
		entries.push(entry);

		entry = {};
		entry.description = "yC31p";
		entry.callback    = "setVideo";
		entry.parameters  = {  video: {name: "videos/yC31p.mp4"} };
		entry.entryColor  = "white";
		entries.push(entry);

		entry = {};
		entry.description = "yC31s";
		entry.callback    = "setVideo";
		entry.parameters  = {  video: {name: "videos/yC31s.mp4"} };
		entry.entryColor  = "orange";
		entries.push(entry);

		entry = {};
		entry.description = "Start Playing";
		entry.callback    = "setColor";
		entry.parameters  = {  playing: true };
		entry.entryColor  = "orange";
		entries.push(entry);

		return entries;
	}
});
