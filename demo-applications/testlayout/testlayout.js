"use strict";

var testlayout = SAGE2_App.extend({
	init: function(data) {
		this.smallwork(data);

		//if(this.checkWindow(ui.clientID) === true){
			this.work(data);
		//}
	},

	getClientDimensions: function(client_id){
		var client_height = 960;

		// there are two columns in the wall
		if (client_id < 5) {
			var client_width = 7680;
			var row_index = client_id;
			var column_index = 0;
		}
		else {
			var client_width = 6720;
			var row_index = Math.abs(5 - client_id);
			var column_index = 1;
		}

		var starting_x = column_index * client_width;
		var starting_y = row_index * client_height;

		var ending_x = (column_index + 1) * client_width;
		var ending_y = (row_index + 1) * client_height;

		return [starting_x, starting_y, ending_x, ending_y, client_width, client_height];

	},

	checkWindow: function(client_id){
		var client_starting_x, client_starting_y, client_ending_x, client_ending_y, client_width, client_height;

		this.client_coordinates = this.getClientDimensions(client_id);
		[client_starting_x, client_starting_y, client_ending_x, client_ending_y, client_width, client_height] = this.client_coordinates;

		var window_starting_x = this.sage2_x;
		var window_starting_y = this.sage2_y;

		var window_ending_x = this.sage2_x + this.sage2_width;
		var window_ending_y = this.sage2_y + this.sage2_height;


		// transform y coordinates to lower end of screen from upper end
		var totalHeight = ui.json_cfg.totalHeight;

		client_starting_y = totalHeight - client_starting_y;
		client_ending_y = totalHeight - client_ending_y;

		window_starting_y = totalHeight - window_starting_y;
		window_ending_y = totalHeight - window_ending_y;

		// check if the rectangles overlap each other
		if ((window_starting_x > client_ending_x) || (client_starting_x > window_ending_x) ){
			return false;
		}

		else if ((window_starting_y < client_ending_y) || (client_starting_y < window_ending_y) ){
			return false;
		}
		
		else {
			return true;
		}

	},

	smallwork: function(data){
		this.SAGE2Init("div", data);
		this.element.id = "div_" + data.id;
		this.element.style.backgroundColor = 'black';

		this.moveEvents = "onfinish";
		this.resizeEvents = "continuous";

		// Trying to add the slider and a small test variable for that
		this.zoomLevel = 10;

		// shamelessly copied from google maps for testing

		this.dragging     = false;
		this.position     = {x: 0, y: 0};
		this.scrollAmount = 0;

		this.map = document.createElement("div");
		this.map.id = "map_" + this.id;
		this.map.style.position = "absolute";
		this.map.style.top = "0";
		this.map.style.bottom = "0";
		this.map.style.width = "100%";
		this.element.appendChild(this.map);

		if ('customLaunchParams' in data){
			var width = data['customLaunchParams'].width;
			var height = data['customLaunchParams'].height;
			this.sendResize(width, height);
			this.resetWindow(width, height);
		}
	},

	work: function(data){
		this.gif = document.createElement("img");
		this.gif.id = "gif_" + this.id;
		this.gif.src = "images/pacman.gif";
		this.gif.style.position = "absolute";
		this.gif.style.top = "50%";
		this.gif.style.left = "50%";
		this.element.appendChild(this.gif);

		this.canvas = document.createElement("canvas");
		this.canvas.id = "canvas_" + this.id;
		this.canvas.width = parseInt(2159*0.5);
		this.canvas.height = parseInt(960*0.5);
		this.element.appendChild(this.canvas);

		this.tsunamidata = {
			bathymetry: 'images/bathymetry.png',
			bathymetryMetadata: {
					zmin: -6709,
					zmax: 10684
			},
			earthquake: [{
					depth: 22900,
					strike: 17,
					dip: 13,
					rake: 108,
					U3: 0.1,
					cn: -36.122,
					ce: -72.898,
					Mw: 9,
					reference: 'center'
			}],
			coordinates: 'spherical',
			waveWidth: parseInt(2159*0.5),
			waveHeight: parseInt(960*0.5),
			xmin: -179.991,
			xmax: 179.675,
			ymin: -71.991,
			ymax: 79.8411,
			isPeriodic: true,
			canvas:this.canvas
	};

	this.output = {
		displayWidth: parseInt(2159*0.5),
		displayHeight: parseInt(960*0.5),
		stopTime: 60 * 60 * 60,
		displayOption: 'heights',
	};

	this.niterations = 0;
	this.isSimulationRunning = false;
	this.dataLoadedCounter = 0;
	this.currentController;

	this.startTime = null;


	this.lifeCycle = {
			dataWasLoaded: (model) => {
					this.gif.style = 'display:none';
					this.changeTitleBar();
					this.dataLoadedCounter++;
					this.startTime = Date.now();
					console.log(this.startTime);
			},

			controllerLoaded: (model, controller) => {
				this.currentController = controller;
				this.currentController.togglePause();
				console.log(this.startTime);
			},

			modelStepDidFinish: (model, controller) => {

					console.log(Date.now());

					if (model.currentTime >= controller.stopTime){
						this.isSimulationRunning = false;
						console.log("Simulation has finished");
					}
					else{
						this.isSimulationRunning = true;
						console.log("Simulation is running");
					}


					if (model.discretization.stepNumber % 1000 == 0) {
							console.log(model.currentTime/60/60, controller.stopTime/60/60);

							this.timestepValue = model.currentTime/60/60;
						
							if (this.timestepValue > 20 && this.timestepValue < 23){
								this.millis = Date.now() - this.startTime;
								console.log(this.millis);
							}
					}

					this.niterations = this.niterations + 1;
					
					// Use this for faster/slower simulation
					if (this.niterations % 70 == 0) {
							this.niterations = 0;
							return false;
					}
					else {
							return true;
					}
			}
	};

	var mapStyle = {
			"version": 8,
			"name": "tsunamilab",
			"metadata": {
				"mapbox:origin": "basic-template-v1",
				"mapbox:autocomposite": true,
				"mapbox:type": "template",
				"mapbox:sdk-support": {
					"js": "0.46.0",
					"android": "6.0.0",
					"ios": "4.0.0"
				}
			},
			"center": [
				0,
				0
			],
			"zoom": 0,
			"bearing": 0,
			"pitch": 0,
			"sources": {
				"composite": {
					"url": "mapbox://celbertin.cjlwegw2207op2vmr8obgv3zg-11dzn,mapbox.mapbox-streets-v7,celbertin.cjle7h2mn0gbm32s6nherqcgd-62fyu",
					"type": "vector"
				},
				"mapbox://mapbox.satellite": {
					"url": "mapbox://mapbox.satellite",
					"type": "raster",
					"tileSize": 256
				},
				"canvas": {
					"type": "canvas",
					"canvas": "canvas_" + this.id,
					"coordinates": [
						[
							-179.9,
							69
						],
						[
							179.6,
							69
						],
						[
							179.6,
							-69
						],
						[
							-179.9,
							-69
						]
					],
					"dimensions": [
						0,
						0,
						this.sage2_width,
						this.sage2_height
					]
				}
			},
			"sprite": "mapbox://sprites/celbertin/cjl86fxxp0epk2sqk9k2di6ip",
			"glyphs": "mapbox://fonts/celbertin/{fontstack}/{range}.pbf",
			"layers": [
				{
					"id": "background",
					"type": "background",
					"layout": {},
					"paint": {
						"background-color": [
							"interpolate",
							[
								"linear"
							],
							[
								"zoom"
							],
							5,
							"hsl(38, 43%, 89%)",
							7,
							"hsl(38, 48%, 86%)"
						]
					}
				},
				{
					"id": "mapbox-satellite",
					"type": "raster",
					"metadata": {},
					"source": "mapbox://mapbox.satellite",
					"layout": {},
					"paint": {}
				},
				{
					"id": "canvas",
					"source": "canvas",
					"type": "raster",
					"paint": {
						"raster-opacity": 0.8
					}
				},
				{
					"id": "subduction",
					"type": "line",
					"source": "composite",
					"source-layer": "subduction",
					"layout": {},
					"paint": {
							"line-color": "hsl(120, 100%, 50%)"
					}
				},

				{
					"id": "country-label",
					"type": "symbol",
					"metadata": {},
					"source": "composite",
					"source-layer": "country_label",
					"minzoom": 1,
					"maxzoom": 8,
					"layout": {
						"text-field": [
							"get",
							"name_en"
						],
						"text-max-width": [
							"interpolate",
							[
								"linear"
							],
							[
								"zoom"
							],
							0,
							5,
							3,
							6
						],
						"text-font": [
							"step",
							[
								"zoom"
							],
							[
								"literal",
								[
									"Roboto Medium",
									"Arial Unicode MS Regular"
								]
							],
							4,
							[
								"literal",
								[
									"Roboto Bold",
									"Arial Unicode MS Bold"
								]
							]
						],
						"text-size": [
							"interpolate",
							[
								"linear"
							],
							[
								"zoom"
							],
							2,
							[
								"step",
								[
									"get",
									"scalerank"
								],
								13,
								3,
								11,
								5,
								9
							],
							9,
							[
								"step",
								[
									"get",
									"scalerank"
								],
								35,
								3,
								27,
								5,
								22
							]
						]
					},
					"paint": {
						"text-halo-width": 1.5,
						"text-halo-color": "hsl(0, 0%, 100%)",
						"text-color": "hsl(0, 0%, 0%)"
					}
				},
				{
					"id": "admin-country",
					"type": "line",
					"metadata": {},
					"source": "composite",
					"source-layer": "admin",
					"minzoom": 1,
					"filter": [
						"all",
						[
							"<=",
							"admin_level",
							2
						],
						[
							"==",
							"disputed",
							0
						],
						[
							"==",
							"maritime",
							0
						]
					],
					"layout": {
						"line-join": "round",
						"line-cap": "round"
					},
					"paint": {
						"line-color": "hsl(0, 0%, 50%)",
						"line-width": [
							"interpolate",
							[
								"linear"
							],
							[
								"zoom"
							],
							3,
							0.5,
							10,
							2
						]
					}
				}
			],
			"created": "2018-08-24T15:54:58.217Z",
			"id": "cjl86fxxp0epk2sqk9k2di6ip",
			"modified": "2018-08-24T16:34:18.879Z",
			"owner": "celbertin",
			"visibility": "public",
			"draft": false
		};

		this.tsunamimodel = new NAMI.app(this.tsunamidata, this.output, this.lifeCycle);

		mapboxgl.accessToken = 'pk.eyJ1IjoiY2VsYmVydGluIiwiYSI6ImNqbDg0N3o1ejBkMjAzdm84b2dyZTE5ZW4ifQ.WIFB9uoghXkhmueTJip_xg';

		this.mapboxElement = new mapboxgl.Map({
				container: 'map_' + this.id,
				style: mapStyle,
				//style: 'mapbox://styles/mapbox/streets-v9',
				maxZoom: 10,
				minZoom: 1
		});



		this.setCamera({camera: this.state.camera});
		// Get the value and update the app as needed
		this.serverDataGetValue("globalCamera", "getValue");
		// Subscribe to the update for this variable
		this.serverDataSubscribeToValue("globalCamera", "subscribeToCamera");


	},

	load: function(date) {
		console.log('testlayout> Load with state value', this.state.value);
		this.refresh(date);
	},

	draw: function(date) {
		//console.log('testlayout> Draw with state value', this.state.value);
	},

	resize: function(date) {
		this.mapboxElement.resize();

		console.log(this.sage2_x + " " + this.sage2_y);
		console.log(this.sage2_width + " " + this.sage2_height);
		
		this.forceUpdateCenter();

		this.refresh(date);
	},

	move: function(date) {
		this.refresh(date);
	},

	quit: function() {
		console.log("Trying to quit");
	},

	changeTitleBar: function(){
		var application_identifier = this.id;
		var text_identifier = application_identifier + "_text";
		var titlebar = document.getElementById(text_identifier);

		console.log(text_identifier);
		console.log(titlebar);

		var earthquake = this.tsunamidata.earthquake[0];

		titlebar.innerHTML = "Depth: " + earthquake.depth + " " +
												 "Strike: " + earthquake.strike + " " +
												 "Dip: " + earthquake.dip + " " +
												 "Rake: " + earthquake.rake + " " +
												 "U3: " + earthquake.U3 + " " +
												 "CN: " + earthquake.cn + " " +
												 "CE: " + earthquake.ce + " " +
												 "MW: " + earthquake.Mw;
	},

	updateCenter: function() {

		var cameraPosition = {
									camera: {
										zoom:this.mapboxElement.getZoom(),
										longitude:this.mapboxElement.getCenter().lng,
										latitude:this.mapboxElement.getCenter().lat
									}
								};

		this.setCamera(cameraPosition);

	},

	forceUpdateCenter: function(cameraPosition){
		var cameraPosition = {
							camera: {
								zoom:1,
								longitude:0,
								latitude:0
							}
						};
		this.setCamera(cameraPosition);
	},

	event: function(eventType, position, user_id, data, date) {
				if (eventType === "pointerPress" && (data.button === "left")) {
					// This only happens the first time					
					if(this.dataLoadedCounter == 1){
						this.currentController.togglePause();
					}

					this.dragging = true;
					this.position.x = position.x;
					this.position.y = position.y;
					this.refresh(date);
				} else if (eventType === "pointerMove" && this.dragging) {

					var delta_x = this.position.x - position.x;
					var delta_y = this.position.y - position.y;

					this.mapboxElement.panBy([delta_x, delta_y],{
          	animate: false
					});

					this.updateCenter();

					this.position.x = position.x;
					this.position.y = position.y;

					this.refresh(date);
				} else if (eventType === "pointerRelease" && (data.button === "left")) {
					this.dragging = false;
					this.position.x = position.x;
					this.position.y = position.y;
					this.refresh(date);
				}

				else if (eventType === "pointerScroll") {

					// Scroll events for zoom
					this.scrollAmount += data.wheelDelta;

					// artificially make the user scroll more to change a zoom level
					if (this.scrollAmount >= 64) {
						// zoom out
						var z = parseInt(this.mapboxElement.getZoom());
						this.mapboxElement.setZoom(z - 1);
						this.scrollAmount -= 64;

						this.updateCenter();
					} else if (this.scrollAmount <= -64) {
						// zoom in
						var z = parseInt(this.mapboxElement.getZoom());
						this.mapboxElement.setZoom(z + 1);
						this.scrollAmount += 64;

						this.updateCenter();
					}
				}

	},

	resetWindow: function(width, height){
		this.sage2_width = width;
		this.sage2_height = height;
	},

	setCamera: function(responseObject) {
		// update the state of this app
		this.state.camera = responseObject.camera;

		// it's a change from a user action, so tell the other app instances
		this.serverDataSetValue("globalCamera", this.state.camera, "Two Color");
		// synchronized the clients of this app
		this.SAGE2Sync(true);

		// update the camera of mapbox
		this.mapboxElement.setCenter([this.state.camera.longitude, this.state.camera.latitude]);
		this.mapboxElement.setZoom(this.state.camera.zoom);
	},

	changeCamera: function(responseObject) {
		// update the state of this app
		this.state.camera = responseObject.camera;
		//console.log("Changing position to this" + this.state.p);

		// update the camera of mapbox
		this.mapboxElement.setCenter([this.state.camera.longitude, this.state.camera.latitude]);
		this.mapboxElement.setZoom(this.state.camera.zoom);

		//this.refresh(responseObject.date);
		// synchronized the clients of this app
		this.SAGE2Sync(true);
	},


	getValue: function(value) {
		//console.log('Got a value', value);
		// Change the camera of this app
		this.changeCamera({camera: value});
	},

	subscribeToCamera: function(newCamera) {
		//console.log('Got an update', newCamera);
		// Change the camera of this app
		this.changeCamera({camera: newCamera});
	},

	resetWindow: function(width, height){
		this.sage2_width = width;
		this.sage2_height = height;
	},

	// close all other instances running except this one
	closeWindows: function(){
		// This is some sort of a global variable
		console.log(applications);

		for (var app in applications) {
			if (app != this.id){
				if (applications[app].application === "testlayout") {
					applications[app]["close"]();
				}
			}
		}

		console.log(this.id);
		console.log("Trying to close a window");
	},

	addWindows: function(messageParameters){

		// close other instances of the application that are currently running on the wall
		this.closeWindows();

		var numWindows = parseInt(messageParameters.clientInput);

		console.log("Trying to add multiple windows");

		// find the number windows wanted
		var numWindows = parseInt(messageParameters.clientInput);

		console.log("Trying to add multiple windows");

		console.log(this.config);

		var totalHeight = this.config.totalHeight;
		var totalWidth = this.config.totalWidth;

		// Actual width after subtracting the header and footer from the wall
		// ***** Important *****
		var headerHeight = 120;
		var totalHeight = this.config.totalHeight - (2* headerHeight);
		// ***** Important *****


		console.log("Current wall resolution: " + totalHeight + " " + totalWidth);
		//console.log("Current window coordinates: " + this.sage2_x + " " + (this.sage2_y-120));
		console.log("Current Window Size: " + this.sage2_height + " " + this.sage2_width);

		var columns = Math.ceil(Math.sqrt(numWindows));
		var fullRows = parseInt(numWindows / columns);
		var orphans = parseInt(numWindows % columns);
		var width =  parseInt(totalWidth/ columns);

		// Adhitya: I know you could have written a ternary operator instead of this if-else. Do not do it.
		if (orphans == 0){
			var height = parseInt(totalHeight / (fullRows));
		}
		else{
			var height = parseInt(totalHeight / (fullRows + 1));
		}

		console.log(columns + " " + fullRows + " " + orphans + " " + width + " " + height);

		// Regardless of whether you have orphans or not
		for (var y = 0; y < fullRows; y++){
			for (var x = 0; x < columns; x++){
				this.launchAppWithValues("testlayout", {"width": width, "height": height}, x*width, (y*height)+((y+1)*headerHeight));
			}
		}

		// Add a row at the bottom if you have orphans
		if (orphans != 0){
			var orphansWidth = orphans * width;
			var widthOffset = (totalWidth - orphansWidth)/2;
			var orphanWindowHeight = headerHeight + (fullRows * headerHeight) + (fullRows * height);

			for(var x = 0; x < orphans; x++){
				var orphanWidth = ((x*width)+widthOffset);
				var orphanHeight = (orphanWindowHeight);
				this.launchAppWithValues("testlayout", {"width": width, "height": height}, orphanWidth, orphanHeight);
			}
		}

		this.close();

	},

	changeEarthquakeDepth: function(messageParameters){

		var newDepth = parseInt(messageParameters.clientInput);
		const newSubfault = Object.assign(this.tsunamidata.earthquake[0], {depth: newDepth});
		const newEarthquake = [newSubfault];

		this.niterations = 0;
		this.tsunamimodel.model.earthquake = newEarthquake;

		// if the simulation is over, then the below line will matter
		// otherwise the nami library will animate on its own

		this.tsunamimodel.controller.animate();

		this.getFullContextMenuAndUpdate();
	},

	changeStopTime: function(messageParameters){
		var newStopTime = parseInt(messageParameters.clientInput);
		this.output.stopTime = newStopTime;

		// get the current depth of the earthquake
		// and then try to mimic changeEarthquake depth, since it clears the canvas

		var oldDepth = (this.tsunamidata.earthquake[0]).depth;
		this.changeEarthquakeDepth({"clientInput": oldDepth});

		this.getFullContextMenuAndUpdate();

	},

	reanimateEarthquake: function(){
		// proxy for animating the same earthquake again
		var oldDepth = (this.tsunamidata.earthquake[0]).depth;
		this.changeEarthquakeDepth({"clientInput": oldDepth});
	},

	setZoom: function(messageParameters){
		var newZoomLevel = parseInt(messageParameters.clientInput);
		this.zoomLevel = newZoomLevel;
		console.log("New zoom level: " + this.zoomLevel);
		this.getFullContextMenuAndUpdate();
	},

	getContextEntries: function() {
		var entries = [];
		var entry = {};

		entry.description = "Number of Simulations: ";
		entry.callback = "addWindows";
		entry.parameters     = {};
		entry.inputField     = true;
		entry.inputFieldSize = 20;
		entry.value = "4";
		entries.push(entry);

		entry = {};
		entry.description = "Simulation Time: ";
		entry.callback = "changeStopTime";
		entry.parameters     = {};
		entry.inputField     = true;
		entry.inputFieldSize = 20;
		entry.value = this.output.stopTime;
		entries.push(entry);

		entry = {};
		entry.description = "Earthquake Depth: ";
		entry.callback = "changeEarthquakeDepth";
		entry.parameters     = {};
		entry.inputField     = true;
		entry.inputFieldSize = 20;
		entry.value = (this.tsunamidata.earthquake[0]).depth;
		entries.push(entry);

		entry = {};
		entry.description = "Earthquake Strike: ";
		entry.callback = "changeEarthquakeStrike";
		entry.parameters     = {};
		entry.inputField     = true;
		entry.inputFieldSize = 20;
		entry.value = (this.tsunamidata.earthquake[0]).strike;
		entries.push(entry);

		entry = {};
		entry.description = "Earthquake Dip: ";
		entry.callback = "changeEarthquakeDip";
		entry.parameters     = {};
		entry.inputField     = true;
		entry.inputFieldSize = 20;
		entry.value = (this.tsunamidata.earthquake[0]).dip;
		entries.push(entry);

		entry = {};
		entry.description = "Earthquake Rake: ";
		entry.callback = "changeEarthquakeRake";
		entry.parameters     = {};
		entry.inputField     = true;
		entry.inputFieldSize = 20;
		entry.value = (this.tsunamidata.earthquake[0]).rake;
		entries.push(entry);

		entry = {};
		entry.description = "Earthquake U3: ";
		entry.callback = "changeEarthquakeU3";
		entry.parameters     = {};
		entry.inputField     = true;
		entry.inputFieldSize = 20;
		entry.value = (this.tsunamidata.earthquake[0]).U3;
		entries.push(entry);

		entry = {};
		entry.description = "Earthquake CN: ";
		entry.callback = "changeEarthquakeCN";
		entry.parameters     = {};
		entry.inputField     = true;
		entry.inputFieldSize = 20;
		entry.value = (this.tsunamidata.earthquake[0]).cn;
		entries.push(entry);

		entry = {};
		entry.description = "Earthquake CE: ";
		entry.callback = "changeEarthquakeCE";
		entry.parameters     = {};
		entry.inputField     = true;
		entry.inputFieldSize = 20;
		entry.value = (this.tsunamidata.earthquake[0]).ce;
		entries.push(entry);

		entry = {};
		entry.description = "Earthquake MW: ";
		entry.callback = "changeEarthquakeMW";
		entry.parameters     = {};
		entry.inputField     = true;
		entry.inputFieldSize = 20;
		entry.value = (this.tsunamidata.earthquake[0]).Mw;
		entries.push(entry);

		entry = {};
		entry.description = "Reanimate Simulation";
		entry.callback = "reanimateEarthquake";
		entry.parameters = {};
		entries.push(entry);

		entry = {};
		entry.description = "Zoom:";
		entry.callback = "setZoom";
		entry.value = this.zoomLevel;
		entry.inputField = true;
		entry.inputType = "range";
		entry.inputUpdateOnChange = true;
		entry.sliderRange = [0, 20];
		entry.parameters = {};
		entries.push(entry);

		return entries;
	}


});
