"use strict";

var mapbox = SAGE2_App.extend({
	init: function(data) {
		this.SAGE2Init("div", data);
		this.element.id = "div_" + data.id;
		this.element.style.backgroundColor = 'black';
		this.resizeEvents = "continuous"; // onfinish
		this.maxFPS = 2.0;
		this.controls.finishedAddingControls();
		this.enableControls = true;

		// shamelessly copied from google maps for testing

		this.dragging     = false;
		this.position     = {x: 0, y: 0};
		this.scrollAmount = 0;


		this.map = document.createElement("div");
		this.map.id = "map_" + this.id;
		this.map.style.position = "absolute";
		this.map.style.top = "0";
		this.map.style.bottom = "0";
		this.map.style.width = "100%";
		this.element.appendChild(this.map);


		if ('customLaunchParams' in data){
			var width = data['customLaunchParams'].width;
			var height = data['customLaunchParams'].height;
			this.sendResize(width, height);
			this.resetWindow(width, height);
		}

		//this.passSAGE2PointerAsMouseEvents = true;

		mapboxgl.accessToken = 'pk.eyJ1IjoiY2VsYmVydGluIiwiYSI6ImNqbDg0N3o1ejBkMjAzdm84b2dyZTE5ZW4ifQ.WIFB9uoghXkhmueTJip_xg';

		this.mapboxElement = new mapboxgl.Map({
				container: 'map_' + this.id,
				//style: mapStyle
				style: 'mapbox://styles/mapbox/streets-v9',
				maxZoom: 10,
				minZoom: 1
		});

		this.setCamera({camera: this.state.camera});
		// Get the value and update the app as needed
		this.serverDataGetValue("globalCamera", "getValue");
		// Subscribe to the update for this variable
		this.serverDataSubscribeToValue("globalCamera", "subscribeToCamera");

	},

	load: function(date) {
		console.log('testlayout> Load with state value', this.state.value);
		this.refresh(date);
	},

	draw: function(date) {
		//console.log('testlayout> Draw with state value', this.state.value);
	},

	resize: function(date) {
		this.mapboxElement.resize();

		console.log(this.sage2_width + " " + this.sage2_height);
		
		this.forceUpdateCenter();

		this.refresh(date);
	},

	move: function(date) {
		this.refresh(date);
	},

	quit: function() {
	},

	updateCenter: function() {

		var cameraPosition = {
									camera: {
										zoom:this.mapboxElement.getZoom(),
										longitude:this.mapboxElement.getCenter().lng,
										latitude:this.mapboxElement.getCenter().lat
									}
								};

		this.setCamera(cameraPosition);

	},

	forceUpdateCenter: function(cameraPosition){
		var cameraPosition = {
							camera: {
								zoom:1,
								longitude:0,
								latitude:0
							}
						};
		this.setCamera(cameraPosition);
	},

	event: function(eventType, position, user_id, data, date) {
				if (eventType === "pointerPress" && (data.button === "left")) {
					this.dragging = true;
					this.position.x = position.x;
					this.position.y = position.y;
					this.refresh(date);
				} else if (eventType === "pointerMove" && this.dragging) {

					var delta_x = this.position.x - position.x;
					var delta_y = this.position.y - position.y;

					this.mapboxElement.panBy([delta_x, delta_y],{
          	animate: false
					});

					this.updateCenter();

					this.position.x = position.x;
					this.position.y = position.y;

					this.refresh(date);
				} else if (eventType === "pointerRelease" && (data.button === "left")) {
					this.dragging = false;
					this.position.x = position.x;
					this.position.y = position.y;
					this.refresh(date);
				}

				else if (eventType === "pointerScroll") {

					// Scroll events for zoom
					this.scrollAmount += data.wheelDelta;

					// artificially make the user scroll more to change a zoom level
					if (this.scrollAmount >= 64) {
						// zoom out
						var z = parseInt(this.mapboxElement.getZoom());
						this.mapboxElement.setZoom(z - 1);
						this.scrollAmount -= 64;

						this.updateCenter();
					} else if (this.scrollAmount <= -64) {
						// zoom in
						var z = parseInt(this.mapboxElement.getZoom());
						this.mapboxElement.setZoom(z + 1);
						this.scrollAmount += 64;

						this.updateCenter();
					}
				}

	},

	resetWindow: function(width, height){
		this.sage2_width = width;
		this.sage2_height = height;
	},

	setCamera: function(responseObject) {
		// update the state of this app
		this.state.camera = responseObject.camera;

		// it's a change from a user action, so tell the other app instances
		this.serverDataSetValue("globalCamera", this.state.camera, "Two Color");
		// synchronized the clients of this app
		this.SAGE2Sync(true);

		// update the camera of mapbox
		this.mapboxElement.setCenter([this.state.camera.longitude, this.state.camera.latitude]);
		this.mapboxElement.setZoom(this.state.camera.zoom);
	},

	changeCamera: function(responseObject) {
		// update the state of this app
		this.state.camera = responseObject.camera;
		//console.log("Changing position to this" + this.state.p);

		// update the camera of mapbox
		this.mapboxElement.setCenter([this.state.camera.longitude, this.state.camera.latitude]);
		this.mapboxElement.setZoom(this.state.camera.zoom);

		//this.refresh(responseObject.date);
		// synchronized the clients of this app
		this.SAGE2Sync(true);
	},


	getValue: function(value) {
		//console.log('Got a value', value);
		// Change the camera of this app
		this.changeCamera({camera: value});
	},

	subscribeToCamera: function(newCamera) {
		//console.log('Got an update', newCamera);
		// Change the camera of this app
		this.changeCamera({camera: newCamera});
	}

});
