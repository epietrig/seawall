"use strict";

var photolayout = SAGE2_App.extend({
	init: function(data) {
		this.smallwork(data);

		if(this.checkWindow(ui.clientID) === true){
			this.work(data);
		}
	},

	// Adhitya: if this application is run on another wall other than wilder
	// then this is the only function that has to be changed
	getClientDimensions: function(client_id){

		//var client_height = 960;
		var client_height = ui.height;

		// there are two columns in the wall
		var row_count = ui.json_cfg.layout.rows;
		

		//if (client_id < 5) {
		if (client_id < row_count) {
			//var client_width = 7680;
			var client_width = ui.width;
			var row_index = client_id;
			var column_index = 0;
		}
		else {
			//var client_width = 6720; //15630 - 960 - 7680
			var client_width = (this.config.totalWidth) - ui.height - ui.width;
			var row_index = Math.abs(row_count - client_id);
			var column_index = 1;
		}

		var starting_x = column_index * client_width;
		var starting_y = row_index * client_height;

		var ending_x = (column_index + 1) * client_width;
		var ending_y = (row_index + 1) * client_height;

		return [starting_x, starting_y, ending_x, ending_y, client_width, client_height];

	},

	checkWindow: function(client_id){
		var client_starting_x, client_starting_y, client_ending_x, client_ending_y, client_width, client_height;

		this.client_coordinates = this.getClientDimensions(client_id);
		[client_starting_x, client_starting_y, client_ending_x, client_ending_y, client_width, client_height] = this.client_coordinates;

		var window_starting_x = this.sage2_x;
		var window_starting_y = this.sage2_y;

		var window_ending_x = this.sage2_x + this.sage2_width;
		var window_ending_y = this.sage2_y + this.sage2_height;


		// transform y coordinates to lower end of screen from upper end
		var totalHeight = ui.json_cfg.totalHeight;

		client_starting_y = totalHeight - client_starting_y;
		client_ending_y = totalHeight - client_ending_y;

		window_starting_y = totalHeight - window_starting_y;
		window_ending_y = totalHeight - window_ending_y;

		// check if the rectangles overlap each other
		if ((window_starting_x > client_ending_x) || (client_starting_x > window_ending_x) ){
			return false;
		}

		else if ((window_starting_y < client_ending_y) || (client_starting_y < window_ending_y) ){
			return false;
		}
		
		else {
			return true;
		}

	},

	// get a small boundary drawn so that sage2 variables are defined
	smallwork: function(data){
		this.SAGE2Init("div", data);

		this.element.id = "div_" + data.id;
		this.element.style.backgroundColor = 'black';

		// magic!
		this.state.randomInteger = Math.floor(Math.random() * Math.floor(100));

		// Check if this was an application launched by another one
		if ('customLaunchParams' in data){
			//console.log("Application launched by some other guy");
			var width = data['customLaunchParams'].width;
			var height = data['customLaunchParams'].height;

			this.state.applicationIndice = data['customLaunchParams'].applicationIndice;

			this.resetWindow(width, height);
			this.sendResize(width, height);
		}
		else{
			var width = this.state.width;
			var height = this.state.height;
		}

		//console.log(data);	
	},

	work: function(data){
		this.setImage();

		this.moveEvents = "onfinish";
		this.resizeEvents = "continuous";
		this.maxFPS = 2.0;

		this.controls.finishedAddingControls();
		this.enableControls = true;
	},



	changeRandomImage: function(randomInteger){
		this.backgroundImage.src = "https://picsum.photos/100/100/?image="+ randomInteger;
	},

	setImage: function(){
		if(!('backgroundImage' in this)){
			this.backgroundImage = document.createElement("img");
			console.log("Yes background image is not present");
		}
		
		this.backgroundImage = document.createElement("img");
		this.backgroundImage.width = this.sage2_width;
		this.backgroundImage.height = this.sage2_height;

		// Adhitya: I will never forget the next line. It took me two weeks to debug just because of this.
		// Adhitya: Who am I kidding? I am the most forgetful person ever. :P
		//this.state.randomInteger = Math.floor(Math.random() * Math.floor(100));

		this.backgroundImage.src = "https://picsum.photos/100/100/?image="+ this.state.randomInteger;

		this.element.appendChild(this.backgroundImage);
		this.changeTitleBar();
	},

	addWindows: function(messageParameters){

		// close other instances of the application that are currently running on the wall
		this.closeWindows();

		var numWindows = parseInt(messageParameters.clientInput);

		console.log("Trying to add multiple windows");

		// find the number windows wanted
		var numWindows = parseInt(messageParameters.clientInput);

		var totalHeight = this.config.totalHeight;

		// Adhitya: The totalWidth inside SAGE2 seems to incorrect
		//var totalWidth = 14400;		
		var totalWidth = (this.config.totalWidth) - ui.height;

		// Actual width after subtracting the header and footer from the wall
		// ***** Important *****
		var headerHeight = ui.upperBar.clientHeight;
		var totalHeight = this.config.totalHeight - (2* headerHeight);
		// ***** Important *****


		//console.log("Current wall resolution: " + totalHeight + " " + totalWidth);
		//console.log("Current window coordinates: " + this.sage2_x + " " + (this.sage2_y-120));
		//console.log("Current Window Size: " + this.sage2_height + " " + this.sage2_width);

		var columns = Math.ceil(Math.sqrt(numWindows));
		var fullRows = parseInt(numWindows / columns);
		var orphans = parseInt(numWindows % columns);
		var width =  parseInt(totalWidth/ columns);

		// Adhitya: I know you could have written a ternary operator instead of this if-else. Do not do it.
		if (orphans == 0){
			var height = parseInt(totalHeight / (fullRows));
		}
		else{
			var height = parseInt(totalHeight / (fullRows + 1));
		}

		//console.log(columns + " " + fullRows + " " + orphans + " " + width + " " + height);

		var applicationIndice = 0;

		// Regardless of whether you have orphans or not
		for (var y = 0; y < fullRows; y++){
			for (var x = 0; x < columns; x++){				
				// Just wait for a bit, so that the alignment is not random 
				this.sleep(0.1);
				this.launchAppWithValues(this.application, {"width": width, "height": height, "applicationIndice": applicationIndice++}, x*width, (y*height)+((y+1)*headerHeight));
				

			}
		}

		// Add a row at the bottom if you have orphans
		if (orphans != 0){
			var orphansWidth = orphans * width;
			var widthOffset = (totalWidth - orphansWidth)/2;
			var orphanWindowHeight = headerHeight + (fullRows * headerHeight) + (fullRows * height);

			for(var x = 0; x < orphans; x++){
				var orphanWidth = ((x*width)+widthOffset);
				var orphanHeight = (orphanWindowHeight);

				this.sleep(0.1);
				this.launchAppWithValues(this.application, {"width": width, "height": height, "applicationIndice": applicationIndice++}, orphanWidth, orphanHeight);
			}
		}

		this.close();

	},

	// close all other instances running except this one
	closeWindows: function(){
		// This is some sort of a global variable
		//console.log(applications);

		for (var app in applications) {
			if (app != this.id){
				if (applications[app].application === this.application) {
					applications[app]["close"]();
				}
			}
		}

		//console.log(this.id);
		console.log("Trying to close a window");
	},



	load: function(date) {
		this.refresh(date);
	},

	draw: function(date) {
	},


	resize: function(date) {
		// Called when window is resized

		// this function maybe called even before an image tag gets created		
		if(typeof this.backgroundImage !== "undefined") {
  			this.backgroundImage.width = this.sage2_width;
			this.backgroundImage.height = this.sage2_height;
		} 
		
		this.refresh(date);
	},

	move: function() {
		this.sage2_x = 0;
		this.sage2_y = 0;
	},

	quit: function() {
		console.log("Trying to quit");
	},

	resetWindow: function(width, height){
		this.sage2_width = width;
		this.sage2_height = height;

		console.log("Window is being reset?");
	},

	resetWindowSize: function(){
		var width = 2900;
		var height = 2900;

		this.sage2_width = width;
		this.sage2_height = height;

		this.sendResize(width, height);	

		console.log("Window is being reset?");
	},

	changeTitleBar: function(){
		var application_identifier = this.id;
		var text_identifier = application_identifier + "_text";
		var titlebar = document.getElementById(text_identifier);

		titlebar.innerHTML = "Identifier: " + application_identifier + " RandomInteger:" + this.state.randomInteger + " indice:" + this.state.applicationIndice + " display: " + ui.clientID;

		console.log(titlebar.innerHTML);
	},


	getContextEntries: function() {
		var entries = [];
		var entry   = {};

		entry = {};
		entry.description = "Number of Simulations: ";
		entry.callback = "addWindows";
		entry.parameters     = {};
		entry.inputField     = true;
		entry.inputFieldSize = 10;
		entry.value = "4";
		entries.push(entry);

		entry = {};
		entry.description = "Reset Window Size";
		entry.callback = "resetWindowSize";
		entry.parameters     = {};
		entries.push(entry);

		entry = {};
		entry.description = "Intersecting Clients";
		entry.callback = "intersectingWindows";
		entry.parameters     = {};
		entries.push(entry);

		entry = {};
		entry.description = "Close Windows";
		entry.callback = "closeWindows";
		entry.parameters     = {};
		entries.push(entry);

		entry = {};
		entry.description = "Print Garbage";
		entry.callback = "printGarbage";
		entry.parameters     = {};
		entries.push(entry);

		return entries;
	},

	event: function(eventType, position, user_id, data, date) {
		// this application currently does not require any events		
		//console.log("event");
	},

	sleep: function(seconds){
    	var waitUntil = new Date().getTime() + seconds*1000;
    	while(new Date().getTime() < waitUntil) true;
	},

	intersectingWindows: function(){
		console.log("trying to print client windows");
		var display_count = ui.json_cfg.rows * ui.json_cfg.columns;

		for (var i = 0; i < 10; i++){
			console.log(i + " " + this.checkWindow(i));
		}
	},

	// best function ever!
	printGarbage: function(){
		console.log(this);
		console.log(ui);
	},


	// close all other instances running except this one
	closeWindows: function(){
		// This is some sort of a global variable
		//console.log(applications);

		for (var app in applications) {
			if (app != this.id){
				if (applications[app].application === this.application) {
					applications[app]["close"]();
				}
			}
		}

		//console.log(this.id);
		console.log("Trying to close all windows");
	},

});
