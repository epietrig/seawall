import numpy as np
import matplotlib.pyplot as plt


def getXYZfromFile(file):
    data = np.loadtxt(file,delimiter=',',skiprows=1)
    x = data[:,3]/1e2
    y = data[:,5]/1e2
    z = data[:,4]/1e2
    return x,y,z


def getGriddedDataFromFile(file):
    from scipy.interpolate import griddata
    x,y,z = getXYZfromFile(file)
    xgrid = np.linspace(-23000,23000,1000)
    ygrid = np.linspace(-12000,12000,1000)
#     xgrid = np.linspace(x.min(), x.max(), 1000)
#     ygrid = np.linspace(y.min(), y.max(), 1000)
    xgrid,ygrid = np.meshgrid(xgrid,ygrid)
    points = np.vstack([x,y]).T
    values = z.copy()
    unique = np.unique(values)
    freq = [np.where(values == unique[i])[0].shape[0] for i in range(unique.shape[0])]
    imaxfreq = np.argmax(freq)
    moda = unique[imaxfreq]
    zgrid = griddata(points,values, (xgrid, ygrid), fill_value=moda)
    zgrid = zgrid - moda
    return xgrid, ygrid, zgrid


if __name__=='__main__':
    files = ["points/ya31.csv", "points/ya32.csv", "points/yb31.csv", "points/yc31.csv"]

    for file in files:
        print('current file:', file)
        xgrid, ygrid, zgrid = getGriddedDataFromFile(file)
        print('x extent: ', xgrid.min(), xgrid.max())
        print('y extent: ', ygrid.min(), ygrid.max())
        print('z extent: ', zgrid.min(), zgrid.max())
        plt.pcolormesh(xgrid, ygrid, zgrid)
        plt.xlabel('x (m)')
        plt.ylabel('y (m)')
        plt.axis('equal')
        plt.colorbar()
        

        
        
        fname = file.split('/')[1]
        fname = 'rectangles/'+fname
        plt.savefig(fname+'.png',dpi=300)
        plt.close()
        np.savetxt(fname, zgrid)
        