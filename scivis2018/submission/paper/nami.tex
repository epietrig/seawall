% !TEX root =  main.tex

\section{\mbox{Asteroid Generated Tsunami Simulation}}
Seawall is based on Nami, a WebGL + Javascript library under development at the Pontifical Catholic University of Chile, that aims to ease the development of efficient tsunami simulation based solutions using Web technologies. This library allows to define different kinds of initial conditions and study their results as time series or scalar fields. Nami's simulator calculates the propagation of tsunami waves in a 2D spherical grid, and then extrapolates these values to estimate their impact on the coast using techniques that are common in Tsunami Early Warning Systems (TEWS). Two different approaches are explored to estimate the initial deformation of the water free surface. The first uses detailed volumetric data to infer the deformation at every point in a 2D numerical grid, and the second uses existing formulas that depend only on global parameters of the earthquake or asteroid causing the tsunami.

\subsection{Nami Simulations}

The tsunami simulator consists of a second-order finite difference approximation of the linear Shallow Water Equations (SWE) on the sphere, that uses a staggered grid in space and a leap-frog scheme in time. These equations are an approximation of the Euler equations, and represent the balance of mass and momentum fluxes in a depth-averaged fashion. They can be written as:

\begin{equation}
    \frac{\partial \eta }{\partial t} + 
\frac{1}{R cos(\theta)} \left(  
  \frac{\partial M}{\partial \lambda} +
   \frac{\partial }{\partial \theta}(N \cos \theta)\right) = 0
\end{equation}

\begin{equation}
\frac{\partial M}{\partial t} + 
\frac{gh}{R \cos \theta} \frac{\partial \eta}{\partial \lambda} = 0
\end{equation}
 \begin{equation}
 \frac{\partial N}{\partial t} + 
\frac{gh}{R} \frac{\partial \eta}{\partial \theta} = 0
 \end{equation}
 
where  $\lambda, \theta$ are the longitude and latitude coordinates of the event, and $t$ is the time dimension; $\eta$ and $(M, N)$ are the 2D fields of free surface deformation and horizontal momentum; $h$ is the bathymetry field, and $g=9.81 m/s^2$ the gravity acceleration. The numerical grid spans the whole globe, except for the poles to avoid grid singularities. Shore lines are considered as fully reflective internal boundaries.

Two different approaches are explored to extract the initial condition from the dataset. The first one uses the water volume fraction 3D field (\verb;v02; in the dataset) to extract the contour surface \verb;v02=1; that is then interpolated into a uniform 2D grid over a subrectangle of the numerical domain. The time frame is chosen according to \cite{ward2000asteroid}, by selecting the instant when most of the surface moves from a downward to an upward direction. The second approach instead, approximates the initial deformation as a parabolic cavity as:
\begin{equation}
	\eta_0(x,y)=
	\begin{cases}
    	-D_c(1-r^2)/R_C^2 & r\leq R_D\\
        0 & r > R_D
    \end{cases}
\end{equation}
where $(x,y)$ is a point located in a cartesian coordinate system whose center is the same as that of the asteroid cavity, and $r = \sqrt{x^2+y^2}$;
$D_C$ is the cavity depth; and $R_C$ and $R_D$ are the inner and outer cavity radii. $R_C, R_D$ and $D_C$ can simply be obtained by fitting this parabola to the data, but we instead obtain it using a simpler formula, used for instance by \cite{ward2000asteroid}:

\begin{equation}
	\begin{array}{cc}
    	D_C = d_c/3 = Q R_I ^{3/4}; & Q = (8\varepsilon V_I^2/9(\rho_w g)^{1/4}
      \label{eq:aproximate_formulas}
    \end{array}
\end{equation}
with $d_c=2R_C$; $R_I, V_I$ the asteroid radius and speed; $\varepsilon$  the fraction of energy transferred from the asteroid to the tsunami; and $\rho_w$  the water mass density. Though $R_I$ is known {\em a priori}, $V_I$ and $\varepsilon$ are calculated from the datasets by taking the respective values and integrating all contributions of $1/2 \rho_w g \eta_0^2$ from the inferred initial surface with the other approach.

Finally, for a given Point Of Interest (POI) located in shallow water with bathymetry $h_{POI} > 0$, we estimate its surface elevation using Green's Law:
\begin{equation}
	\begin{array}{c}
    	\eta_{POI} = \eta_d (h_d/h_{POI})^{0.25}
    \end{array}
\end{equation}
where $\eta_d$ is the water elevation at the closest grid point located in deep water with bathymetry $h_d$. Though this approach oversimplifies the processes behind tsunami inundation, it has proven sufficient to provide useful values compatible with the orders of magnitude expected by TEWS.


\subsection{Data preprocessing}

We use data from \cite{patchett2017} to demonstrate our results. Each asteroid simulation provided in the ensemble dataset has three stages of evolution: approach, impact and aftermath. For each simulation, we are interested in extracting parameters about the impact of the asteroid on the surface of the water. We process all time-steps provided, with the intent of finding the deepest impression of the asteroid on the water surface, as explained in the previous section. To infer parameters of equation \eqref{eq:aproximate_formulas}, we use the average velocity of the asteroid.
