\documentclass{vgtc}                          % final (conference style)

\ifpdf%                                % if we use pdflatex
  \pdfoutput=1\relax                   % create PDFs from pdfLaTeX
  \pdfcompresslevel=9                  % PDF Compression
  \pdfoptionpdfminorversion=7          % create PDF 1.7
  \ExecuteOptions{pdftex}
  \usepackage{graphicx}                % allow us to embed graphics files
  \DeclareGraphicsExtensions{.pdf,.png,.jpg,.jpeg} % for pdflatex we expect .pdf, .png, or .jpg files
\else%                                 % else we use pure latex
  \ExecuteOptions{dvips}
  \usepackage{graphicx}                % allow us to embed graphics files
  \DeclareGraphicsExtensions{.eps}     % for pure latex we expect eps files
\fi%

%% it is recomended to use ``\autoref{sec:bla}'' instead of ``Fig.~\ref{sec:bla}''
\graphicspath{{figures/}{pictures/}{images/}{./}} % where to search for the images

\usepackage{microtype}                 % use micro-typography (slightly more compact, better to read)
\PassOptionsToPackage{warn}{textcomp}  % to address font issues with \textrightarrow
\usepackage{textcomp}                  % use better special symbols
\usepackage{mathptmx}                  % use matching math font
\usepackage{times}                     % we use Times as the main font
\renewcommand*\ttdefault{txtt}         % a nicer typewriter font
\usepackage{cite}                      % needed to automatically sort the references
\usepackage{tabu}                      % only used for the table example
\usepackage{booktabs}                  % only used for the table example
\usepackage[utf8]{inputenc}            % used for Jose's name
\usepackage[T1]{fontenc}               % used for Jose's name
\usepackage{amsmath}
%% If you are submitting a paper to a conference for review with a double
%% blind reviewing process, please replace the value ``0'' below with your
%% OnlineID. Otherwise, you may safely leave it at ``0''.
\onlineid{0}

%% declare the category of your paper, only shown in review mode
\vgtccategory{Research}

%% Paper title.

\title{Comparative Visualization of Deep Water Asteroid Impacts\\on Ultra-high-resolution Wall Displays with Seawall}

\author{Adhitya Kamakshidasan\thanks{e-mail: adhitya.kamakshidasan@inria.fr} \hspace{1em} José Galaz\thanks{e-mail: jose.galaz@inria.cl} \hspace{1em} Rodrigo Cienfuegos\thanks{e-mail: director@cigiden.cl} \hspace{1em} Antoine Rousseau\thanks{e-mail: antoine.rousseau@inria.fr} \hspace{1em} Emmanuel Pietriga\thanks{e-mail: emmanuel.pietriga@inria.fr}\\
{\scriptsize \hspace{1.5em} Inria, France  \hspace{6em} Inria, Chile \hspace{4.5em} CIGIDEN, Chile  \hspace{5.5em} Inria, France  \hspace{5.5em} Inria, France}\\
}
%        {\scriptsize \centering Inria, France} %
%     \scriptsize Inria, Chile %
%     \scriptsize CIGIDEN, Chile %
%     \scriptsize Inria, France
%     {\scriptsize \centering Inria, France} }



%% A teaser figure can be included as follows, but is not recommended since
%% the space is now taken up by a full width abstract.
\teaser{
  \includegraphics[width=\columnwidth]{images/2018_08_01_1763.jpg}
  \caption{Multiple simulations running simultaneously on the WILDER ultra-wall. The high display capacity of this interactive surface makes it possible to show, for each of the simulations: a planet-wide view showing the propagation of the asteroid-generated tsunami on the globe, a close-up on the region of impact, showing a simulation of one or more scalar fields, parameters of the simulation.\vspace{2em}}
  \label{fig:teaser}
}

%% Abstract section.
\abstract{We use a ultra-high-resolution wall display to make it easier for analysts to visually compare and contrast different simulations from a deep water asteroid impact ensemble dataset. Thanks to their very high pixel density, these physically large surfaces enable users to display multiple simulations simultaneously, juxtaposing multiple perspectives on the data for each of them. Playback of the different simulations can be synchronized. Changes of perspective on the data, made interactively in one view by users, can also be applied automatically to the corresponding views in the other simulations.
} % end of abstract

\CCScatlist{
  \CCScatTwelve{Human-centered computing}{Visu\-al\-iza\-tion}{Scientific visualization}{};
  \CCScatTwelve{Human-centered computing}{Visu\-al\-iza\-tion}{Geographic visualization}{}
}

%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%% START OF THE PAPER %%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

%params: airburst, asteroid diameter and angle of entry
% 11 scalar fields: pressure, temperature, velocity in x, velocity in y, velocity in z, sound speed, AMR grid refinement level, material ID, volume fraction of water and volume fraction of asteroid

\begin{document}

\firstsection{Introduction}

\maketitle

The deep water asteroid impact dataset~\cite{patchett2017} has two main characteristics that make it difficult to visualize: 1. it is an ensemble dataset, consisting of multiple simulations of the same phenomenon but with different parameters (airburst, asteroid diameter, and angle of entry); and 2. each simulation is a multi-variate dataset, consisting for the most part of scalar fields (pressure, temperature, velocity along the three axes, {\em etc.}) amenable to volumetric rendering or 2D visualization (slicing). It is actually possible to derive additional data from the original dataset, that can be of significant interest to analysts who want to understand the influence of the parameters on the possible generation of a tsunami, and the characteristics of that tsunami.

\begin{figure*}[t]
\centering
  \includegraphics[width=2\columnwidth]{images/2018_08_01_1773.jpg}
  \caption{Multiple simulations of the same asteroid (same airburst, size and angle of entry) impacting the Earth in different locations. From left to right: Pacific ocean, Caribbean sea, Indian ocean, South Atlantic ocean.}
  \label{fig:smDL}
\end{figure*}


The problem of visualizing combinations of scalar fields in a given simulation has already been explored by Samsel {\em et al.}~\cite{samsel2017}. Rather than proposing other volumetric renderings for these specific data, we focus here on a complementary question: how to efficiently display multiple simulations in the ensemble simultaneously to facilitate compare \& contrast tasks; and how to display related views on each one of these simulations, to enable analysts to gain additional insights about the influence of the considered parameters.

Displaying multiple simulations simultaneously consumes significant screen real-estate. In previous communications about the study of asteroid-generated tsunamis (AGT), Patchett, Samsel and colleagues typically show up to a maximum of four simulations simultaneously. But displaying multiple complementary views on multiple simulations, all simultaneously, is near impossible on regular workstations. Thanks to their very high pixel density over a large physical surface, cluster-driven ultra-high-resolution wall displays, or ultra-walls for short~\cite{nancel2015}, can accommodate much more data~\cite{nancel2011}. For instance, the wall display depicted in Figure~\ref{fig:teaser} has a total resolution of 14,400 × 4,800 pixels. The other wall in our lab, WILD, has a total resolution of 20,480 × 6,400 pixels. Larger walls have appeared since then. Potential applications include, {\em e.g.}, astronomical data analysis~\cite{pietriga2016}, neuroimagery~\cite{wild2012} and medical imagery analysis~\cite{ruddle2016}, road traffic management~\cite{prouzeau2016}, air quality monitoring~\cite{pietriga2017} and other geovisualizations. We describe a proof-of-concept visualization environment for the AGT data, called Seawall. The proof-of-concept presented here can display multiple simulations from the deep water asteroid impact dataset, and can show complementary views for each of the simulations on screen, as illustrated in Figure~\ref{fig:teaser}.


%One way to tackle this issue is by employing Tiled display walls, that can display multiple simulations from ensemble datasets at the same time. Such displays can represent the data with a high level of detail while at the same time retaining context : users can transition from an overview of the data to a detailed view simply by physically moving in front of the wall display. Wall displays also offer good support for collaborative work, enabling multiple users to simultaneously visualize and interact with the data. [Adhitya: Ask Emmanuel to cite usages of such displays in other scientific exploration]

%In this work, we present the exploration of deep water asteroid impacts by using ultra-high-resolution wall-sized displays. Along with showcasing the features of our framework for understanding the evolution of tsunami during such impacts, we also provide an analysis of various impact situations.


%-----------------------------------------------------------------------------
%Basic Introduction usecases: (a) Motivate the usage of wall-displays for Tsunami visualization. (b) Motivate the necessity of performing Tsunami simulations [Look at the Emmanuel's seawall document]. (c) Motivate the complexity of making sense of ensemble simulations [Look at Julien Tierny's latest VIS paper + colormap HCI paper by data suppliers].

%Adhitya: IMHO there are three main advantages that we have over other contestants
%1) Usage of the wall for interaction of asteroid impacts
%2) Capabilites of the nami library for fast real time rendering of impacts
%3) Analysis of impacts on the globe

\input{overview}
\input{nami}

%\section{Analysis}
%[Mainly plots that Jose and Adhitya will come up with]
%(a) Major events that happen in the water surface before/during/after impact of 
%(b) Talk about the time for propagation of waves to reach a shore
%(c) How does an airburst affect wave size? [say this from the impact cavity figure from different simulations]
%(d) How does the angle of impact affect the crater, AGT, near and far field?
%[Adhitya: ask Jose if crater and impact cavity mean the same thing]
%(e) What are the near/far field effects of Tsunamis in our case? [
%(e.1) Is the \{vertical angle, horizontal angle, asteroid size, etc\} important in the near and/or far field?
%--> we could for instance study the time series at a fixed point of interest fixed and move the asteroid location and compare things.
%(e.2) How much do we "win" from the detailed initial condition v/s the aproximate formulas in terms of impact estimation?
%(f) The waveforms to trace the surface of the ocean over a cross section
%(g) Plot of energy imparted under different circumstances

%\section{Conclusion}
%
%\acknowledgments{
%The authors wish to thank A, B, and C. This work was supported in part by
%a grant from XYZ.}

\bibliographystyle{abbrv-doi}

\bibliography{main}

\end{document}


