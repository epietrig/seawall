# Contributing

* Features, bugs, releases should be pushed using git-flow branching model.
* Info regarding installation, setup and use explanations can be found here: https://danielkummer.github.io/git-flow-cheatsheet/

# Installing Nami
* This repo should be cloned with
```
git clone --recursive  https://github.com/jgalazm/Nami
```


* If working on an existing clone, update the submodule with

```
git submodule update --init --recursive
```

Then, at `Nami/` run 
```
npm run build
```
to obtain the `nami.js` bundle.


