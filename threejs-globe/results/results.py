import numpy as np
import matplotlib.pyplot as plt
from matplotlib.colors import LinearSegmentedColormap

root = 'ya31/'
"""

custom colormap

"""

def saveEnergyPlot(root):
    x = np.linspace(-179.99, 179.67, 2159)
    y = np.linspace(-79.99, 79.84, 960)
    x,y = np.meshgrid(x,y)
    hmax = np.loadtxt(root+'/tlab2D (1)',delimiter=',',skiprows=1).reshape((960, 2159))
    arrivals = np.loadtxt(root+'/tlab2D',delimiter=',',skiprows=1).reshape((960, 2159))

    k = 3
    m = 30
    plt.figure(figsize=(6*k,2*k))
    plt.pcolormesh(x,y,hmax,vmax=m,vmin=0.0, cmap = plt.cm.gist_stern)
    plt.colorbar()
    c5 = plt.contour(x,y,arrivals/(60*60), np.arange(0,30,5), colors = 'w', linewidths = 0.4)
    c1 = plt.contour(x,y,arrivals/(60*60), np.arange(0,30,1), colors = 'w', linewidths = 0.2)
    plt.clabel(c5, inline=1, fontsize=10)
    ax = plt.gca()
    ax.set_aspect(1)
    ax.set_xlabel(u'Lon °')
    ax.set_ylabel(u'Lat °')
    fname = 'maxheight_'+root[:-1]+'.png'
    plt.savefig(fname, dpi=300)
    print('finished ',fname)
    plt.close()


# for root in roots: 


def saveTimeSeriesPlot(root):
    import pandas as pd
    tseries = pd.read_csv(root+'/tseries')
    # tseries.set_index('#seconds')

    f = plt.figure(figsize=(9,6))
    for i in range(7):
        column = tseries.columns[i+1]
        f.add_subplot(5,2,i+1)
        plt.plot(tseries['#seconds']/60/60,tseries[column] ,lw=0.4)
        plt.title(column[:-3])
        plt.ylabel('height [m]')
        plt.grid()
    plt.xlabel('t (hours)')

    plt.tight_layout()
    plt.savefig('tseries_'+root+'.png',dpi=300, bbox_inches='tight')
    plt.close()

roots = ['ya31', 'ya31_caribbean', 'ya31_indeanocean', 'ya31_southatlantic', 'yb31']

for root in roots:
    saveTimeSeriesPlot(root)
    saveEnergyPlot(root)