import numpy as np
import matplotlib.pyplot as plt
x = np.linspace(-1,1,500)
y = np.linspace(-1,1,500)
x,y = np.meshgrid(x,y)
z = -100.0*np.exp(-(x*x+y*y)/25)
np.savetxt('initialRectangle.csv',z,  delimiter=',')
plt.pcolormesh(x,y,z)
plt.colorbar()