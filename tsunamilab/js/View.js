var TsunamiView = function (params) {
  var containerID0 = params.containerID0;
  var videoElement = params.videoElement;
  var bbox = params.bbox;
  var currentPin = undefined;
  var rotate = true;
  var slaveCameraOffsets = [];

  var createViewer = function (canvasID) {
    Cesium.BingMapsApi.defaultKey = 'AhuWKTWDw_kUhGKOyx9PgQlV3fdXfFt8byGqQrLVNCMKc0Bot9LS7UvBW7VW4-Ym';
    var viewer = new Cesium.Viewer(canvasID, {
      // sceneMode: Cesium.SceneMode.SCENE2D,
      imageryProvider: new Cesium.createTileMapServiceImageryProvider({
        url: 'uploads/apps/tsunamilab/node_modules/cesium-tsunamilab/Build/Cesium/Assets/Textures/NaturalEarthII'
      }),
      baseLayerPicker: false,
      animation: false,
      fullscreenButton: false,
      scene3DOnly: true,
      geocoder: false,
      homeButton: false,
      infoBox: false,
      sceneModePicker: false,
      selectionIndicator: false,
      timeline: false,
      shadows: false,
      skyAtmosphere: false,
      navigationHelpButton: false,
      navigationInstructionsInitiallyVisible: false
    });
    viewer.scene.globe.enableLighting = false;
    viewer.scene.screenSpaceCameraController.inertiaSpin = 0;
    viewer.scene.screenSpaceCameraController.inertiaTranslate = 0;
    viewer.scene.screenSpaceCameraController.inertiaZoom = 0;
    viewer.imageryLayers.addImageryProvider(new Cesium.createTileMapServiceImageryProvider({
      url: 'uploads/apps/tsunamilab/node_modules/cesium-tsunamilab/Build/Cesium/Assets/Textures/NaturalEarthII'
    }));
    viewer.cesiumWidget.screenSpaceEventHandler.removeInputAction(Cesium.ScreenSpaceEventType.LEFT_DOUBLE_CLICK);


    viewer.scene.globe.depthTestAgainstTerrain = false;
    viewer.scene.skyBox.show = false;
    viewer.scene.screenSpaceCameraController.minimumZoomDistance = 1e3 * 10;
    viewer.scene.screenSpaceCameraController.maximumZoomDistance = 1e9 * 10;


    var videoLayer = viewer.entities.add({
      rectangle: {
        coordinates: Cesium.Rectangle.fromDegrees(bbox[0][0], Math.max(bbox[0][1], -89.99999),
          bbox[1][0], Math.min(bbox[1][1], 89.99999)),
        height: 0,
        material: videoElement,
        asynchronous: true
      }
    });
    videoLayer.rectangle.material.transparent = true;

    return viewer;
  }

  var viewer = createViewer(containerID0);

  var viewers = [viewer];

  var previousTime = Date.now();


  var flyTo = function (viewer, lat, lng, scale) {
    var scale = scale ? scale : 3;
    viewer.camera.flyTo({
      destination: Cesium.Rectangle.fromDegrees((lng - 5) - 3 * scale, (lat - 5) - 3 * scale,
        (lng + 5) + 3 * scale, (lat + 5) + 3 * scale)
    });
  }

  var setColormap = function (cmap, labelsMap, canvas) {
    var cbwater = canvas;

    //setup colorbar
    if (typeof cmap == "string") {
      var watermap = getColormapArray(cmap, 1, 0);
    } else {
      var watermap = cmap;
    }
    cbwater.width = Math.min(window.innerWidth / 4, 300);
    cbwater.height = 50;

    colorbar(watermap, labelsMap, cbwater);
  }

  return {
    viewer: viewer,
    setColormap: setColormap,
    rotate: rotate,
  };
}
