let viewer;
let videoLayer;
var videoElement = document.getElementById('videoElement');
let tsunamiView;
let restartTimer;

/* 
Model setup
*/
let data = {
    bathymetry: 'uploads/apps/tsunamilab/assets/bathymetry.png',
    bathymetryMetadata: {
        zmin: -6709,
        zmax: 10684
    },
    earthquake: [{
        depth: 22900,
        strike: 17,
        dip: 13.0,
        rake: 108.0,
        U3: 0.0,
        cn: -36.122,   //centroid N coordinate, e
        ce: -72.898,
        Mw: 9,
        reference: 'center'
    }],
    coordinates: 'spherical',
    waveWidth: parseInt(2159),
    waveHeight: parseInt(960),
    displayWidth: parseInt(2159),
    displayHeight: parseInt(960),
    xmin: -179.99166666666667,
    xmax: 179.67499999999998,
    ymin: -79.991666666666646,
    ymax: 79.841666666666654,
    isPeriodic: true
}

let output = {
    stopTime: 30 * 60 * 60,
    displayOption: 'heights',
    loop: true
};

let niterations = 0;

let lifeCycle = {
    dataWasLoaded: (model) => {
        var videoElement = document.getElementById('videoElement');
        var stream = model.canvas.captureStream(15);
        videoElement.srcObject = stream;
        var options = { mimeType: 'video/webm' };

        document.body.appendChild(model.canvas);
        tsunamiView = TsunamiView({
            containerID0: 'cesiumContainer',
            videoElement: videoElement,
            bbox: [[data.xmin, data.ymin], [data.xmax, data.ymax]]
        });
    },

    modelStepDidFinish: (model, controller) => {
        // videoLayer.rectangle.material = model.canvas;

        if (model.discretization.stepNumber % 1000 == 0) {
            // console.log(model.currentTime/60/60, controller.stopTime/60/60);
        }
        niterations = niterations + 1;

        if (niterations % 10 == 0) {
            niterations = 0;
            return false;
        }
        else {
            return true;
        }

    },

    modelSimulationWillStart: (model, controller) => {
        controller.paused = true;
        
        // para salir del lock de stepnumber = 0 y paused en primera iteración
        model.discretization.stepNumber += 1;


        model.displayPColor();

        clearTimeout(restartTimer);

        restartTimer = setTimeout(() =>{
            controller.paused = false;
        }, 1000);

    
    }
}

let thismodel = new NAMI.app(data, output, lifeCycle);


/* clock */ 

function writeTimeStamp(time) {
    var timetext = "";

    var hours = Math.floor(time / 60 / 60),
      minutes = Math.floor((time - (hours * 60 * 60)) / 60),
      seconds = Math.round(time - (hours * 60 * 60) - (minutes * 60));
    var timetext = timetext.concat(hours + ':' +
      ((minutes < 10) ? '0' + minutes : minutes) + ':' +
      ((seconds < 10) ? '0' + seconds : seconds));
    var hoursText = ((hours < 10) ? '0' + hours : hours)
    var minutesText = ((minutes < 10) ? '0' + minutes : minutes)

    document.getElementById('timer').innerHTML = `Time elapsed1 </br> 
    <strong> ${timetext} </strong>`;
    
}

setTimeout(() => {
    //timeout helps to wait for assets to be loaded
    tsunamiView.viewer.clock.onTick.addEventListener(() => {
            writeTimeStamp(thismodel.model.currentTime);
    });
}, 1500);
