# tsunamilab
This is a minimal version of wwww.tsunamilab.cl.
Please run it under Chrome or Chromium.
To install use npm 
```
    npm install
```

To run start a local server:
```
    python -m http.server
```
# known issues

* The simulation layer skips many steps between frames: Enable "Optimize background video playback" under `chrome://flags`
