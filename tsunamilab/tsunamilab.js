// simple test program for cesium
// Shamelessly stolen from https://stackoverflow.com/a/15292190
function loadscripts(async, root) {
    if (async === undefined) {
        async = false;
    }

    var scripts = [];
    var _scripts = [
        root+'js/nami.js',
        root+'assets/slabs.js',
        root+'js/View.js',
        root+'js/main.js'
    ];
    for (var s in _scripts) {
        scripts[s] = document.createElement('script');
        scripts[s].type = 'text/javascript';
        scripts[s].src = _scripts[s];
        scripts[s].charset = "utf-8"
        scripts[s].async = async;
    }
    var loadNextScript = function() {
        var script = scripts.shift();
        var loaded = false;
        document.head.appendChild(script);
        script.onload = script.onreadystatechange = function() {
            var rs = this.readyState;
            if (rs && rs != 'complete' && rs != 'loaded') return;
            if (loaded) return;
            loaded = true;
            if (scripts.length) {
                loadNextScript();
            } else {
                // done
            }
        };
    };
    loadNextScript();
}

// function for adding css into the head
function addCSS(url, callback) {
    var fileref = document.createElement("link");

    if (callback) {
        fileref.onload = callback;
    }

    fileref.setAttribute("rel", "stylesheet");
    fileref.setAttribute("type", "text/css");
    fileref.setAttribute("href", url);
    document.head.appendChild(fileref);
}

var tsunamilab = SAGE2_App.extend({
    init: function(data) {

        console.log(document);

        //addCSS("node_modules/cesium/Build/Cesium/Widgets/widgets.css");
        //addCSS("css/tsunamilab.css");

        // Create div into the DOM
        this.SAGE2Init("div", data);
        // Set the DOM id
        this.element.id = "div_" + data.id;


        var video = document.createElement('video');
        video.id = 'videoElement';
        video.style = 'display:none';
        this.element.appendChild(video);

        // make tags for adding the cesium into the document
        var cesiumDiv = document.createElement("div");
        cesiumDiv.id = 'cesiumContainer';
        this.element.appendChild(cesiumDiv);


        var timer = document.createElement("div");
        timer.id = 'timer';
        timer.innerHTML = 'Time elapsed</br><strong> 00:00:00 </strong>';
        this.element.appendChild(timer);

        var self = this;

        // load the scripts one after the other
        loadscripts(undefined, root);



        // move and resize callbacks
        this.resizeEvents = "continuous"; // onfinish
        // this.moveEvents   = "continuous";

        // SAGE2 Application Settings
        //
        // Control the frame rate for an animation application
        this.maxFPS = 2.0;
        // Not adding controls but making the default buttons available
        this.controls.finishedAddingControls();
        this.enableControls = true;
    },

    load: function(date) {
        console.log('tsunamiwall> Load with state value', this.state.value);
        console.log("really loaded");
        this.refresh(date);
    },

    draw: function(date) {
        console.log('tsunamiwall> Draw with state value', this.state.value);
    },

    resize: function(date) {
        // Called when window is resized
        this.refresh(date);
    },

    move: function(date) {
        // Called when window is moved (set moveEvents to continuous)
        this.refresh(date);
    },

    quit: function() {
        // Make sure to delete stuff (timers, ...)
    }
});